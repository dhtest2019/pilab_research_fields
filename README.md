# 飞行器智能感知与控制实验室 - 研究方向

实验室围绕飞行器开展环境感知、空间认知、控制等方面的研究工作，研究工作的体系架构如下图所示：

![research-system](images/research_system.png)



研究工作介绍可以查阅：

* [《无人机实时地图》- 实验室研究整体介绍](http://www.adv-ci.com/blog/download/2022-%E6%97%A0%E4%BA%BA%E6%9C%BA%E5%AE%9E%E6%97%B6%E5%9C%B0%E5%9B%BE.pdf)  ([视频](https://www.bilibili.com/video/BV17G4y187JY/))
* [实验室研究成果视频](https://www.bilibili.com/video/BV1a5411j7kH/)
* [发表论文列表](http://www.adv-ci.com/blog/publications/)




实验室Wiki, SummerCamp, Seminar等材料：
* [>>> 实验室Wiki <<< （内部资料）](https://gitee.com/pi-lab/Wiki)
* [>>> PI-Lab实验室SummerCamp <<< （内部资料）](https://gitee.com/pi-lab/SummerCamp)
* [>>> 实验室程序、项目、教程、文档列表 <<< （内部资料）](https://gitee.com/pi-lab/Wiki/blob/master/ProgramList.md)
* [实验室Seminar](https://gitee.com/pi-lab/seminar)  （[视频](https://www.bilibili.com/video/BV1uK4y1Y7et)）





部分研究方向如下所列：

* [《集群无人机编队控制与协同导航》](https://gitee.com/pi-lab/research_cluster_formation_nav): 学习并研究集群无人机的编队控制，协同导航定位
* [《语义定位与地图》](https://gitee.com/pi-lab/research_semantic_localization)：主要研究如何通过构建认知地图的方式，实现空间定位等
* [《SLAM》](https://gitee.com/pi-lab/research_deep_SLAM)： 主要研究如何融合传统的SLAM与深度学习SLAM，从而模拟人类的环境感知
* [《在线学习与增量学习》](https://gitee.com/pi-lab/research_online_learning)： 研究如何实现模型训练与推理融合一体，从而应对新的场景与变化
* [《差异检测》](https://gitee.com/pi-lab/research_change_detection)：研究如何有效对比不同时刻的图像，判断语义层面的差异和变化
* [《智能控制》](https://gitee.com/pi-lab/intelligent-control)： 主要研究如何通过机器学习等人工智能方法实现类人的智能控制
* [《无人机与地面机器人的协同环境感知与控制》](https://gitee.com/pi-lab/research_air_gound_collaboration)：研究将机器人与无人机有机融合，使用无人机来进行快速的环境感知并实时传输给机器人实现协调任务执行



## 1. 如何做研究

关于如何做研究，请仔细阅读[《如何做研究》](tips/README.md)

想要取得好的研究成果，很好的编程、数学、思维能力必不可少，但刚开始的时候各项能力都比较弱，会处处受挫，所以不要着急，每个人需要根据自己的特点，找到一个突破口，慢慢从这个突破口取得成绩并获得成就感。例如你觉得理论能力相对好，可以先从理论创新开始，然后补其他编程基础的短板；如果自己觉得自己的编程能力比较强，可以先从修改程序入手，逐步加入新的想法，然后逐步学习理论知识。

由于研究生期间的时间比较短，而所需要学习的东西非常多，因此合理的时间分配是十分重要的。此外对所做的研究需要找到相关资料、了解已有哪些方法、问题有哪些等。因此需要在开始研究之后，制定自己的[《研究计划》](tips/research_method/Guidelines_for_ResearchPlanning.md)。制定计划的时候，不用贪多求全，可以先把已经知道的写好，通过研究过程认识的加深不断完善，从而最终完成研究计划和研究本身。更多的内容请参考[《如何做研究》](tips/README.md)



## 2. 学习研究的前奏

在开始研究之前，需要将下列基础知识的学习、练习完成，从而加快后续研究的效率。

1. 首先需要明确自己的目标是什么，这样前行的过程才不会迷茫。本实验室致力于研究无人机、机器人的智能环境感知与控制，需要学习的[《知识、技能、思维方法》](tips/terminal/Targets.md)比较多，请思考如何在有限的时间、精力下尽可能达到目标？
2. 研究不仅仅是时间的付出，更多的是学习、琢磨研究的方法等，因此需要先学习一下：
    - [《学习成功之道》](tips/Tao_of_Success)  ([视频](https://www.bilibili.com/video/BV1h44y1e75C/))
    - [《如何做研究》](tips/README.md)
3. 编程在研究过程非常重要，比较强的编程能力能够快速将想法实现，在相关研究开始之前，先学习一下编程、数据结构、算法、Debug、常用工具等。课程主页在：[《一步一步学编程》](https://gitee.com/pi-lab/learn_programming)
4. 相关研究的基础的自学：
    - [飞行器智能感知与控制实验室 - 入门教程 (SummerCamp) （内部资料）](https://gitee.com/pi-lab/SummerCamp)  ([视频](https://www.bilibili.com/video/BV1dq4y1M7i8/))
    - 以及对应的练习题: [飞行器智能感知与控制实验室 - 入门教程配套练习题](https://gitee.com/pi-lab/SummerCampHomework)
5. 机器学习越来越多的应用到各个研究方向，是一项必备的研究技能
    - [《机器学习与人工智能》 - 教程](https://gitee.com/pi-lab/machinelearning_notebook)
    - [《机器学习与人工智能》 - 作业](https://gitee.com/pi-lab/machinelearning_homework)



## 3. 教程

### 3.1 一步一步、基础教程
* 研究方法论
    - [《学习成功之道》](tips/Tao_of_Success)   ([视频](https://www.bilibili.com/video/BV1h44y1e75C/))
    - [《如何做研究》](tips/README.md)
    - [《人生中转站 - 人生目标与能力模型》](tips/terminal/README.md)
* 学术研究
    - [开题、中期、答辩手续](https://gitee.com/pi-lab/Wiki/tree/master/research_life)
    - [《本科毕业设计指南》](https://gitee.com/pi-lab/graduation_project_guide)， [《学士论文LaTex模版与示例》](https://gitee.com/pi-lab/template_bachelor)
    - [《硕士研究与毕业指南》](https://gitee.com/pi-lab/master_project_guide)， [《硕士论文LaTex模版与示例》](https://gitee.com/pi-lab/template_master)
    - [《博士研究与毕业指南》](https://gitee.com/pi-lab/doctor_project_guide)
* 实验室研究活动
    - [开题、中期、答辩手续](https://gitee.com/pi-lab/Wiki/tree/master/research_life)
    - [《实验室研究成果视频》](https://www.bilibili.com/video/BV1a5411j7kH/)
    - [《发表论文列表》](http://www.adv-ci.com/blog/publications/)
    - [《实验室Seminar》](https://gitee.com/pi-lab/seminar) ([视频](https://www.bilibili.com/video/BV1uK4y1Y7et))
    - [《实验室SummerCamp》](https://gitee.com/pi-lab/SummerCamp)
    - [《实验室公开课》](https://gitee.com/pi-lab/open-course) ([视频](https://www.bilibili.com/video/BV1oy4y1S78z))
    - [《实验室读书会》](https://gitee.com/pi-lab/SummerCamp/blob/master/BookReading.md)
* 写作等基本技能
    - [《如何找资料、如何收集资料、如何看论文》](https://gitee.com/pi-lab/Wiki/tree/master/tips/research_method/CollectMaterials.md)
    - [《一步一步学写作》](https://gitee.com/pi-lab/Wiki/tree/master/tips/learn_writting/README.md)
    - [《如何写论文》](https://gitee.com/pi-lab/Wiki/tree/master/tips/paper_writting/README.md)
    - [《如何审论文》](https://gitee.com/pi-lab/Wiki/tree/master/tips/paper_writting/paper_review/README.md)
    - [《如何回复审稿意见》](https://gitee.com/pi-lab/Wiki/tree/master/tips/paper_writting/paper_response/README.md)
    - [《如何申请专利》](https://gitee.com/pi-lab/Wiki/tree/master/tips/patent/README.md)
    - [《如何申请基金》](https://gitee.com/pi-lab/Wiki/tree/master/tips/funding_proposal/README.md)
* 教程、编程、技能
    - [《一步一步学编程》](https://gitee.com/pi-lab/learn_programming)
    - [《一步一步学SLAM》](https://gitee.com/pi-lab/learn_slam)
    - [《一步一步学ROS》](https://gitee.com/pi-lab/learn_ros)
    - [《一步一步学硬件》](https://gitee.com/pi-lab/learn_hardware)
    - [《一步一步学飞控》](https://gitee.com/pi-lab/research_flight_controller)
    - [《一步一步学控制》](https://gitee.com/pi-lab/learn_control)
    - [《90分钟学Python》](https://gitee.com/pi-lab/machinelearning_notebook/tree/master/0_python)
    - [《90分钟学Arduino》](https://gitee.com/pi-lab/learn_hardware/tree/master/Arduino)
    - [《跟海龟学 Python和数学》](https://gitee.com/pi-lab/python_turtle)
    - 机器学习教程与作业
        - [《机器学习教程》](https://gitee.com/pi-lab/machinelearning_notebook)
        - [《机器学习课程作业》](https://gitee.com/pi-lab/machinelearning_homework)
    - [《飞行器信息信息课程设计》](https://gitee.com/aircraft-is-design/aircraft-information-system-design) 
    - [《实现、编程方法论》](tips/research_method/README.md)
* 视频拍摄与剪辑、项目、产品技能
    - [《一步一步学视频拍摄与剪辑》](https://gitee.com/pi-lab/learn_video)
    - [《一步一步学项目管理》](https://gitee.com/pi-lab/learn_project_management)
    - [《一步一步学产品开发》](https://gitee.com/pi-lab/learn_product_development)



### 3.2 资源文件等

* [实验室Wiki （内部资料）](https://gitee.com/pi-lab/Wiki) ， [实验室Wiki (内网) （内部资料）](http://192.168.1.3/PI_LAB/Wiki)
* [实验室代码、项目、资源列表（全）（内部资料）](https://gitee.com/pi-lab/Wiki/blob/master/ProgramList.md)
* [实验室研究基础图书（PILAB-Book）](https://gitee.com/pi-lab/pilab-book)
* [resources-research](https://gitee.com/pi-lab/resources-research) , [resources-research (内网)](http://192.168.1.3/PI_LAB/resources-research) 
* [resources-book](https://gitee.com/pi-lab/resources) , [resources-book (内网)](http://192.168.1.3/PI_LAB/resources) 
* [resources-fonts](https://gitee.com/pi-lab/resources-fonts) , [resources-fonts (内网)](http://192.168.1.3/PI_LAB/resources-fonts)



### 3.3 工具教程

* [Linux](https://gitee.com/pi-lab/learn_programming/tree/master/6_tools/linux)
* [Git](https://gitee.com/pi-lab/learn_programming/tree/master/6_tools/git)
* [Markdown](https://gitee.com/pi-lab/learn_programming/tree/master/6_tools/markdown)
* [CMake](https://gitee.com/pi-lab/learn_programming/tree/master/6_tools/cmake)




## 4. 软件、代码、数据

* [>>> 实验室代码、项目、资源列表（全）<<< （内部资料）](https://gitee.com/pi-lab/Wiki/blob/master/ProgramList.md)
* 软件和代码的总览可在[Source/Data](http://www.adv-ci.com/blog/source/)找到全部的列表
* 代码实例片段可以[Code Cook](https://gitee.com/pi-lab/code_cook)中找到（内部资料）
* 示例程序与项目[Code Cook - Demo Projects](https://gitee.com/pi-lab/code_cook/blob/master/DemoProjects.md)（内部资料）

