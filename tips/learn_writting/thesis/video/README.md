# 研究成果的演示视频



一个视频顶1000张图，一张图顶1000句话！视频是最形象生动的展示。为了更好的让其他人理解、知道自己的研究思路、内容、创新点、成果，最好制作一个演示视频。这个视频可以介绍自己研究工作的思路、框架、内容、成果演示等等。

需要在答辩的时候展示自己的视频，让大家更容易理解所做的工作。可以在答辩开始放自己研究介绍的视频，也可以在最后总结的时候放视频。

制作视频之前，可以借鉴学习其他人的视频，例如

* [Map2DFusion - 无人机实时二维地图](https://www.bilibili.com/video/BV1Bg4y1s7Xj)
* [ClusterFusion - 集群无人机实时建图](https://www.bilibili.com/video/BV1684y1P77f)

- [水空巡飞弹-哪吒F](https://www.bilibili.com/video/BV1fs4y1H7iK)
- [D2SLAM: Decentralized and Distributed Collaborative Visual-inertial SLAM](https://www.bilibili.com/video/BV1eY4y1Q7j1)
- [无人机集群协同控制飞行试验](https://www.bilibili.com/video/BV1PN411c7yv)

通过仔细分析示例视频，对比分析自己的研究内容、亮点等，构思自己的视频框架、内容等。



## 要点

为了让人更好的掌握自己的工作内容、创新点等，视频制作的时候需要注意一下几点：

* 视频不要太长，最好控制在1分钟以内

* 视频需要一个封面，写清楚课题名称，作者
* 最好有清晰的结构
* 加上说明，解释，方便观看者理解
* 在快放的地方，写清楚倍速，例如 3x



视频最好加上一个封面，选一个比较有代表性的图，写清楚课题名称，作者

![img](images/cover.jpg)



每个章节、部分开始的时候，能清楚地展示出来

![img](images/section.jpg)





需要有一些文字注解，方便读者理解

![img](images/annotation.jpg)



可以加上字幕，对当前的视频内容做一下解释

![img](images/annotation-2.jpg)



如果播放速度快的话，可以把快放的速度标注出来，例如"Speed X4"

![img](images/playspeed-description.jpg)



## 参考资料

* [一步一步学视频](https://gitee.com/pi-lab/learn_video)