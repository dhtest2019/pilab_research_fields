# 智能系统（PI-LAB）实验室的原则与制度

智能系统实验室致力于研究无人系统的感知、决策等智能算法与系统，为实验室更好的发展，要求所有愿意加入实验室的学生必须遵守实验室的有关原则和制度。

## 学习研究方面
1. 人工智能方面的研究课题比较多，实验室尽可能在尊重各自研究兴趣的基础上安排各自的研究课题。但是需要每个服从整体的研究安排。研究生期间具体做哪方面的研究其实并不重要，重要的是通过做一个研究学会分析、解决问题的能力，而具体会那个领域的知识其实相对来说并不是十分重要。
2. 需要根据自己实际情况，安排好实际学习基础知识、做研究方面的平衡。
3. 学习和研究是一个逐步深入的过程，要求各位同学根据自己的实际情况，从相对简单的练手项目开始着手，在做的过程，通过解决一些问题，并反思总结，学会分析、解决问题的能力。


## 制度
1. 实验室是一个学习研究地方，禁止玩游戏等和学习研究无关的事情。如果需要让大脑休息可以做体育锻炼、看看书等。
2. 所安排的工作需要准时完成，在做的过程需要及时反馈。
