# 如何编写研究计划？

学习和研究过程中，做好自己的计划编排对后续研究顺利开展起到关键的作用。通过前期自己搜集资料、阅读、思考，从而基本了解研究对象、特点、发展趋势等，从而为后续的研究工作开展起到积极的作用。不论你的研究领域是什么，你选择了什么样的方法，所有的研究计划必须解决以下问题：你打算完成什么，为什么你要做这件事，以及你打算怎样去完成它。

大家在开展研究工作之前，需要把下面所列的问题、工作等认真完成。按照下面所列的提纲，把各项内容写出，并且在研究开展过程，及时修改并上传到git仓库，从而方便后续研究开展不断反思。如果把所有的内容都想清楚并列出来太难，可以把已经想清楚的写出来，逐步完善。


## 1. 研究背景
* 其他研究者已经哪些工作？
* 目前已有的工作有哪些优点、缺点？
* 从什么渠道去找资料？找代码？
* 有没有找到相关的代码？


## 2. 研究动力 
* 研究课题的意义何在？
* 根据自己的理解，从哪些方面可以得到提升，有哪些新的结果可以预期得到？
* 目前已有的研究的结果能否提高？通过什么途径提高？


## 3. 设计研究主题 
* 选择哪些问题？ 这个问题的难度如何？
* 这个问题中的可变的量是什么？
* 能够获得什么样的可能结果？
* 把问题准确描述出来，并能列出关键的步骤。


## 4. 假设
* 提议的研究的预期结果是什么？
* 基于你的理解、你的猜测（可能的结果）是什么？
* 这项研究有连续的里程碑（研究过程分成几个阶段）吗？
* 每个阶段的预期结果是什么？
* 如果实际结果与你的猜测不同怎么办？


## 5. 方法论
* 你如何证明这个假设是正确的？或者你如何证明这个假设是错误的？
* 你需要设计一个实验来验证假设或者方法？实验的代价大不大？
* 你需要收集哪些数据？使用什么方法来分析这些数据？
* 你需要什么数学理论？有那些方法、算法能够应用到自己的研究？


## 6. 时间规划 
* 你的研究可能得出什么结论？
* 需要多长时间才能得出基本的结果？
* 是否有循序渐进的过程？每一个过程、步骤需要花费的时间、代价、风险等。


## 7. 研究技能
* 编程能力是否能够？
* 相关理论知识掌握的情况？


## Reference
* 基于Ming Zuo老师的原始版本，加入了一些补充。


